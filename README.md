# led_strips_web

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).


## Config
Config File: `.env` Variables:
- `BACKEND_URL`: Location of backend (example `http://192.168.0.249:5000/`)
- `NUXT_PORT`: Port (example `3000`)
- `NUXT_HOST`: Host (example `0.0.0.0`)
